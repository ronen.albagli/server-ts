"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const router = require("./router/router");
const bodyParser = require("body-parser");
class App {
    constructor() {
        this.express = express();
        this.express.use(bodyParser.json());
        this.express.use(bodyParser.urlencoded({ extended: true }));
        this.express.use('/', router);
    }
}
exports.default = new App().express;
//# sourceMappingURL=server.js.map