"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const elastic_controller_1 = require("../elastic/elastic.controller/elastic.controller");
// Elastic router
const router = express.Router();
router.route('/').get((req, res) => {
    res.status(200).send('Server Running');
});
router.route('/search').post(elastic_controller_1.default.search);
router.route('/api/insert').post(elastic_controller_1.default.insert);
module.exports = router;
//# sourceMappingURL=elastic.router.js.map