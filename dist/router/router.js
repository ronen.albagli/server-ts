"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const elasticRouter = require("./elastic.router");
// App router  
const router = express.Router();
router.use('/', elasticRouter);
module.exports = router;
//# sourceMappingURL=router.js.map