"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv").config();
const server_1 = require("./server");
const Logger_1 = require("./util/Logger");
const port = process.env.PORT || 5100;
server_1.default.listen(port, err => {
    if (err) {
        Logger_1.default.error(`Server unable to run - ${err}`);
    }
    Logger_1.default.info("server Running on port " + port);
    return console.log(`server is listening on ${port}`);
});
//# sourceMappingURL=index.js.map