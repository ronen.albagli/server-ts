"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const elastic_configuration_1 = require("../elastic-configuration/elastic-configuration");
const elastic_model_1 = require("../elastic-model/elastic-model");
const Logger_1 = require("../../util/Logger");
class ElasticController {
    constructor() {
        this.ES = elastic_configuration_1.default;
    }
    // Start searching for Data in elastic.
    search(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { index, data } = req.body;
            const elasticRes = yield elastic_model_1.default.searchDoc(index, data);
            return res.status(200).send(elasticRes);
        });
    }
    // Start insertion to elastic.
    insert(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { index, data } = req.body;
            try {
                const elasticRes = yield elastic_model_1.default.insertDoc(index, data);
                return res.status(201).send(elasticRes);
            }
            catch (error) {
                Logger_1.default.error(`FATAL - cannot insert doc to elastic ${error}`);
            }
        });
    }
}
const ESCtrl = new ElasticController();
exports.default = ESCtrl;
//# sourceMappingURL=elastic.controller.js.map