"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const elastic_configuration_1 = require("../elastic-configuration/elastic-configuration");
const util_1 = require("../../util/util");
const Logger_1 = require("../../util/Logger");
class ElasticModel {
    /**
     * Function for insert customer to specific index in elastic.
     * @param index - The index name to preform search on.
     * @param data - Document data.
     */
    insertCustomer(index, data) {
        return __awaiter(this, void 0, void 0, function* () {
            const indexName = util_1.findFromCustomerList(index);
            return elastic_configuration_1.default.es.index({
                index: indexName,
                body: data
            });
        });
    }
    generateCustomerDocsArray(data) {
        const documentsArr = [];
        const segments = data.Segments;
        delete data.Segments;
        segments && segments.forEach(url => {
            const obj = Object.assign({}, data, { FullUrl: url });
            documentsArr.push(obj);
        });
        return documentsArr;
    }
    /**
     * Search by domain name.
     * @param indexName - The index name to preform search on.
     * @param param - Params to search for.
     */
    searchDoc(indexName, param) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield elastic_configuration_1.default.es.search({
                index: indexName,
                type: "domain",
                body: {
                    query: {
                        match: {
                            'WhoIS.Domain.Name': {
                                query: param,
                            }
                        }
                    }
                }
            });
        });
    }
    /**
     *
     * @param indexName - The index name to send to.
     * @param data - Document data.
     */
    insertDoc(indexName, data) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const dataArr = Array.isArray(data) ? data : [data];
                let responses;
                const promiseArr = [];
                for (let i = 0; i < dataArr.length; i++) {
                    dataArr[i].RecordInfo = {
                        Collected: new Date().toISOString()
                    };
                    const customer = dataArr[i].Customer && dataArr[i].Customer;
                    if (customer) {
                        delete dataArr[i].Customer;
                        const docsArr = this.generateCustomerDocsArray(dataArr[i]);
                        docsArr.forEach(doc => {
                            promiseArr.push(this.insertCustomer(customer, doc));
                        });
                        delete dataArr[i].Segments;
                    }
                    // Field for preview the document in elastic.
                    promiseArr.push(elastic_configuration_1.default.es.index({
                        index: indexName,
                        id: dataArr[i].WhoIS.Domain.Name,
                        body: dataArr[i]
                    }));
                }
                responses = yield Promise.all(promiseArr);
                Logger_1.default.info(`documents was inserted successfully `);
                return responses;
            }
            catch (error) {
                Logger_1.default.info(`FATAL - error happens while trying to insert to elastic - ${error}`);
                return error;
            }
        });
    }
}
const elasticModel = new ElasticModel();
exports.default = elasticModel;
//# sourceMappingURL=elastic-model.js.map