"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const AWS = require("aws-sdk");
const connectionClass = require("http-aws-es");
const elasticsearch = require("elasticsearch");
const credentials = new AWS.EnvironmentCredentials("AWS");
class ElasticSearchInitialization {
    constructor() {
        this.es = this.initiateElasticInstance();
    }
    /**
     * Initiate elastic connection.
     */
    initiateElasticInstance() {
        return new elasticsearch.Client({
            host: process.env.ELASTICSEARCH_DOMAIN,
            log: "error",
            connectionClass: connectionClass,
            amazonES: {
                credentials: credentials
            }
        });
    }
}
// Create new instance on elastic connection and use it over the app.
const es = new ElasticSearchInitialization();
exports.default = es;
//# sourceMappingURL=elastic-configuration.js.map