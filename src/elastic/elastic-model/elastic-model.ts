import ElasticSearchInitialization from '../elastic-configuration/elastic-configuration';
import { findFromCustomerList } from '../../util/util'
import logger from '../../util/Logger';
class ElasticModel {
    /**
     * Function for insert customer to specific index in elastic.
     * @param index - The index name to preform search on.
     * @param data - Document data.
     */
    async insertCustomer(index: any, data: any) {
        const indexName = findFromCustomerList(index);
        return ElasticSearchInitialization.es.index({
            index: indexName,
            body: data
        })
    }

    generateCustomerDocsArray(data: any) {
        const documentsArr = [];
        const segments = data.Segments;
        delete data.Segments;
        segments && segments.forEach(url => {
            const obj = { ...data, FullUrl: url };
            documentsArr.push(obj);
        });
        return documentsArr;
    }

    /**
     * Search by domain name.
     * @param indexName - The index name to preform search on.
     * @param param - Params to search for.
     */
    async searchDoc(indexName: string, param: string) {
        return await ElasticSearchInitialization.es.search({
            index: indexName,
            type: "domain",
            body: {
                query: {
                    match: {
                        'WhoIS.Domain.Name': {
                            query: param,
                        }
                    }
                }
            }
        });
    }

    /**
     * 
     * @param indexName - The index name to send to.
     * @param data - Document data.
     */
    async insertDoc(indexName: string, data: elasticMainData) {
        try {
            const dataArr = Array.isArray(data) ? data : [data];
            let responses;
            const promiseArr = [];
            for (let i = 0; i < dataArr.length; i++) {
                dataArr[i].RecordInfo = {
                    Collected: new Date().toISOString()
                }
                const customer = dataArr[i].Customer && dataArr[i].Customer;
                if (customer) {
                    delete dataArr[i].Customer;
                    const docsArr = this.generateCustomerDocsArray(dataArr[i])
                    docsArr.forEach(doc => {
                        promiseArr.push(this.insertCustomer(customer, doc))
                    })
                    delete dataArr[i].Segments;
                }
                // Field for preview the document in elastic.

                promiseArr.push(
                    ElasticSearchInitialization.es.index({
                        index: indexName,
                        id: dataArr[i].WhoIS.Domain.Name,
                        body: dataArr[i]
                    })
                );
            }
            responses = await Promise.all(promiseArr);
            logger.info(`documents was inserted successfully `);
            return responses;
        } catch (error) {
            logger.info(
                `FATAL - error happens while trying to insert to elastic - ${error}`
            );
            return error;
        }
    }
}
const elasticModel = new ElasticModel();
export default elasticModel;