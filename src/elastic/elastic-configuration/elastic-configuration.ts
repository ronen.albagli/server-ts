import * as AWS from 'aws-sdk';
import * as connectionClass from 'http-aws-es';
import * as elasticsearch from 'elasticsearch';
const credentials = new AWS.EnvironmentCredentials("AWS");

class ElasticSearchInitialization {
    public es;
    constructor() {
        this.es = this.initiateElasticInstance();
    }

    /**
     * Initiate elastic connection.
     */
    initiateElasticInstance() {
        return new elasticsearch.Client({
            host: process.env.ELASTICSEARCH_DOMAIN,
            log: "error",
            connectionClass: connectionClass,
            amazonES: {
                credentials: credentials
            }
        });
    }
}

// Create new instance on elastic connection and use it over the app.
const es = new ElasticSearchInitialization();
export default es;