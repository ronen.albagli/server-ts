import ElasticSearchInitialization from '../elastic-configuration/elastic-configuration';
import ElasticModel from '../elastic-model/elastic-model';
import logger from '../../util/Logger';

class ElasticController {
    private ES: any;
    constructor() {
        this.ES = ElasticSearchInitialization;
    }
    
    // Start searching for Data in elastic.
    async search(req, res) {
        const { index, data } = req.body;
        const elasticRes = await ElasticModel.searchDoc(index, data);
        return res.status(200).send(elasticRes);
    }
    
    // Start insertion to elastic.
    async insert(req, res) {
        const { index, data } = req.body as insertRequestType;
        try {
            const elasticRes = await ElasticModel.insertDoc(index, data);
            return res.status(201).send(elasticRes);
        } catch (error) {
            logger.error(`FATAL - cannot insert doc to elastic ${error}`);
        }
    }
}
const ESCtrl = new ElasticController();
export default ESCtrl;