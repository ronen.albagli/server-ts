type elasticMainData = {
    WhoIS: WhoIS,
    Derived: Derived,
    Infrastructures: Infrastructures,
    RecordInfo: RecordInfo,
    Customer?: Number,
    Segments?: Array<String>
    collectedDate?:string;
}

type WhoIS = {
    Contacts: Contacts,
    Domain: Domain,
}

type insertRequestType = {
    index: string,
    data: elasticMainData,
}

type Contacts = {
    Admin: Admin,
    Billing: Billing,
    Technical: Technical,
    Registrant: Registrant,

    Registrar: Registrar,
}

type Registrar = {
    Name: string,
}

type Address = {
    Street: Array<string>,
    City: string,
    State: string,
    PostalCode: string,
}

type Fax = {
    Number: string,
    Ext: string
}
type Telephone = {
    Number: string,
    Ext: string,
}

type Admin = {
    Email: string,
    Name: string,
    Organization: string,
    Address: Address,
    Fax: Fax,
    Telephone: Telephone
}

type Billing = {
    Email: string,
    Name: string,
    Organization: string,
    Address: Address,
    Fax: Fax,
    Telephone: Telephone
}

type Technical = {
    Email: string,
    Name: string,
    Organization: string,
    Address: Address,
    Fax: Fax,
    Telephone: Telephone
}

type Registrant = {
    Email: string,
    Name: string,
    Organization: string,
    Address: Address,
    Fax: Fax,
    Telephone: Telephone
}

type Domain = {
    Name: string,
    TLD: string,
    Created: Date,
    Updated: Date,
    Expired: Date,
    NameServers: string
}

type Infrastructures = {
    Alive: boolean,
    IP: string,
    NameServers: Array<string>
}


type Derived = {
    Adversary: Adversary,
}

type Adversary = {
    Profile: Profile,
    Target: Target,
    Entity: Entity
}

type Profile = {
    Name: string,
}

type Target = {
    Geo: Geo
}

type Geo = {
    Name: string,
    Location: object
}

type Entity = {
    Alternatives: Array<any>,
    Primary: Primary
}

type Primary = {
    Geo: Array<string>,
    Name: string,
    Sector: string,
    Terms: Array<string>
}

type RecordInfo = {
    Collected: any
}