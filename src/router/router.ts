import * as express from 'express';

import * as elasticRouter from './elastic.router';

// App router  
const router = express.Router();

router.use('/', elasticRouter);



module.exports = router;
