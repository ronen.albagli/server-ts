import * as express from 'express';
import ESCtrl from '../elastic/elastic.controller/elastic.controller';

// Elastic router
const router = express.Router();

router.route('/').get((req,res)=>{
    res.status(200).send('Server Running')
});
router.route('/search').post(ESCtrl.search);
router.route('/api/insert').post(ESCtrl.insert);



module.exports = router;

