const customerList = {
  1: {
    customerId: 1,
    company: 'Nike',
    slug: 'nike'
  },

  2: {
    customerId: 2,
    company: 'cambia Healths',
    slug: 'cambia'
  },
  3: {
    customerId: 3,
    company: 'BBT',
    slug: 'bbt'
  }
};

/**
 * Bind customer id to customer name.
 * @param id - customer id
 */
export const findFromCustomerList = (id: string) => {
  return customerList[id].slug;
};
