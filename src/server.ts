import * as express from 'express'
import * as router from './router/router';
import * as bodyParser from 'body-parser';

class App {
  public express

  constructor() {
    this.express = express()
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: true }));
    this.express.use('/', router);
  }
}

export default new App().express