require("dotenv").config();
import app from "./server";
import logger from "./util/Logger";

const port = process.env.PORT || 5100;

app.listen(port, err => {
  if (err) {
    logger.error(`Server unable to run - ${err}`);
  }
  logger.info("server Running on port " + port);
  return console.log(`server is listening on ${port}`);
});
